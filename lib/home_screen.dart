import 'package:blackjack_app/card_widget.dart';
import 'package:blackjack_app/config.dart';
import 'package:blackjack_app/models/a_card.dart';
import 'package:blackjack_app/models/hand.dart';
import 'package:blackjack_app/models/player.dart';
import 'package:blackjack_app/providers/game_manager.dart';
import 'package:blackjack_app/providers/player_role.dart';
import 'package:blackjack_app/widgets/half_circular_text.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flip_card/flip_card_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => GameManager(Player("Phuc Nguyen", BigInt.from(600))),
      builder: (context, widget) => const PlayingScreen(),
    );
  }
}

class PlayingScreen extends StatefulWidget {
  const PlayingScreen({
    Key? key,
  }) : super(key: key);
  @override
  State<PlayingScreen> createState() => _PlayingScreenState();
}

class _PlayingScreenState extends State<PlayingScreen> {
  late GameManager provider;
  @override
  void initState() {
    super.initState();
    showDialogNow(const SizedBox(
      width: 150,
      height: 40,
      child: TextField(
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
              hintText: "Your Name",
              hintStyle: TextStyle(color: Colors.white),
              border: UnderlineInputBorder())),
    ));
    provider = context.read<GameManager>()..addListener(showResultDialog);
  }

  @override
  void dispose() {
    super.dispose();

    provider.removeListener(showResultDialog);
  }

  showResultDialog() {
    if (provider.result != null) {
      showDialogNow(Text(
        provider.result.toString(),
        style: TextStyle(color: Colors.white),
      ));
    }
  }

  showDialogNow(Widget widget) async {
    await Future.delayed(Duration.zero);
    showAnimatedDialog(
      context: context,
      barrierColor: Colors.black26,
      builder: (BuildContext context) {
        final size = MediaQuery.of(context).size;
        return ChangeNotifierProvider<GameManager>.value(
          value: provider,
          child: Center(
            child: Material(
              color: Colors.transparent,
              child: SizedBox(
                width: size.width * 0.6,
                height: size.height * 0.3,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    widget,
                    const SizedBox(height: 10),
                    ElevatedButton(
                        onPressed: () async {
                          Navigator.pop(context);
                          await Future.delayed(const Duration(seconds: 1));
                          await provider.initGame(size);
                        },
                        child: Text("Start")),
                    // Text(provider.result)
                  ],
                ),
              ),
            ),
          ),
        );
      },
      animationType: DialogTransitionType.size,
      curve: Curves.fastOutSlowIn,
      duration: const Duration(seconds: 1),
    );
  }

  showResult() async {
    // await Future.delayed(Duration.zero);
    showAnimatedDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        final size = MediaQuery.of(context).size;
        return ChangeNotifierProvider<GameManager>.value(
          value: provider,
          child: Center(
            child: Material(
              color: Colors.transparent,
              child: SizedBox(
                width: size.width * 0.6,
                height: size.height * 0.3,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: size.width * 0.5,
                      height: 40,
                      child: const TextField(
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              hintText: "Your Name",
                              hintStyle: TextStyle(color: Colors.white),
                              border: UnderlineInputBorder())),
                    ),
                    const SizedBox(height: 10),
                    ElevatedButton(
                        onPressed: () async {
                          Navigator.pop(context);
                          await Future.delayed(const Duration(seconds: 1));
                          await provider.initGame(size);
                        },
                        child: Text("Start")),
                  ],
                ),
              ),
            ),
          ),
        );
      },
      animationType: DialogTransitionType.size,
      curve: Curves.fastOutSlowIn,
      duration: const Duration(seconds: 1),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final centerLeftCard = size.width / 2 - widthCard / 2;
    final cennterBottomCard = size.height / 2;
    // final playerInfo =
    //     context.select<GameManager, Player>((provider) => provider.playerInfo);
    // final playerCardList =
    //     context.select<GameManager, Hand>((provider) => provider.player.hand);
    // final dealerCardList =
    //     context.select<GameManager, Hand>((provider) => provider.dealer.hand);
    print("rebuild parents");

    return Scaffold(
      body: SafeArea(
          child: Container(
        height: double.infinity,
        width: double.infinity,
        color: Colors.green.shade700,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              left: centerLeftCard,
              bottom: cennterBottomCard,
              child: Image.asset(
                "assets/card_face_down.png",
                width: widthCard,
              ),
            ),
            Positioned(
              left: centerLeftCard + widthCard * 0.25 - 10,
              bottom: cennterBottomCard - widthCard - 20,
              child: Container(
                height: widthCard,
                width: widthCard * 0.5 + 20,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.amber, width: 3),
                    borderRadius: BorderRadius.circular(8)),
              ),
            ),
            const HalfCircularText(),
            Positioned(
              bottom: 0,
              right: 0,
              child: Container(
                width: size.width * 0.4,
                height: size.height * 0.07,
                padding: const EdgeInsets.fromLTRB(7, 7, 2, 2),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.amber, width: 2),
                  borderRadius:
                      const BorderRadius.only(topLeft: Radius.circular(20)),
                ),
                child: Column(
                  children: [
                    // Text(
                    //   playerInfo.name +
                    //       "123123j21k3j12k3l".replaceAll('', '\u200B'),
                    //   overflow: TextOverflow.ellipsis,
                    //   style: TextStyle(color: Colors.amber, fontSize: 16),
                    // ),
                    // Text("Wallet: ${playerInfo.wallet.toString()}",
                    //     style: TextStyle(color: Colors.amber, fontSize: 14)),
                  ],
                ),
              ),
            ),
            AnimatedPositioned(
                duration: const Duration(milliseconds: 500),
                left: context.select<GameManager, GameState>(
                            (provider) => provider.gameCurrentStatus) !=
                        GameState.init
                    ? centerLeftCard + widthCard * 0.255
                    : size.width - 30,
                bottom: context.select<GameManager, GameState>(
                            (provider) => provider.gameCurrentStatus) !=
                        GameState.init
                    ? cennterBottomCard - widthCard - 10
                    : 0,
                child: Image.asset(
                  'assets/chip.png',
                  width: 30,
                )),
            AnimatedPositioned(
                duration: const Duration(milliseconds: 500),
                left: context.select<GameManager, GameState>(
                            (provider) => provider.gameCurrentStatus) !=
                        GameState.init
                    ? centerLeftCard + widthCard * 0.25
                    : size.width - 30,
                bottom: context.select<GameManager, GameState>(
                            (provider) => provider.gameCurrentStatus) !=
                        GameState.init
                    ? cennterBottomCard - widthCard + 10
                    : 0,
                child: Image.asset(
                  'assets/chip.png',
                  width: 30,
                )),
            PlayerBuild(
                centerLeftCard: centerLeftCard,
                cennterBottomCard: cennterBottomCard),
            DealerBuild(
                centerLeftCard: centerLeftCard,
                cennterBottomCard: cennterBottomCard),
            Positioned(
                bottom: size.height * 0.13,
                right: 5,
                child: playerActionWidget()),
            Positioned(
                bottom: heightCard + 30,
                child: Text(
                  provider.playerScore,
                  style: const TextStyle(
                    fontSize: 20,
                    color: Colors.amber,
                    fontStyle: FontStyle.italic,
                  ),
                )),
            Positioned(
              top: heightCard + 30,
              child: Text(
                provider.dealerScore,
                style: const TextStyle(
                  fontSize: 20,
                  color: Colors.amber,
                  fontStyle: FontStyle.italic,
                ),
              ),
            ),
            context.read<GameManager>().player.isSplitedCard
                ? Positioned(
                    bottom: 10,
                    left: 5,
                    child: Column(
                      children: [
                        InkWell(
                          onTap: () {
                            context
                                .read<GameManager>()
                                .playerSwapHand(CurrentHand.c1);
                          },
                          child: Container(
                            height: 25,
                            width: 25,
                            decoration: const BoxDecoration(
                                color: Colors.amber, shape: BoxShape.circle),
                            child: const Center(
                              child: Text(
                                "1",
                                style: TextStyle(color: Colors.red),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 10),
                        InkWell(
                          onTap: () {
                            context
                                .read<GameManager>()
                                .playerSwapHand(CurrentHand.c2);
                          },
                          child: Container(
                            height: 25,
                            width: 25,
                            decoration: const BoxDecoration(
                                color: Colors.amber, shape: BoxShape.circle),
                            child: const Center(
                              child: Text(
                                "2",
                                style: TextStyle(color: Colors.red),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ))
                : const SizedBox.shrink()
          ],
        ),
      )),
    );
  }

  playerActionWidget() {
    final isPlayerTurn =
        context.watch<GameManager>().gameCurrentStatus == GameState.playerTurn;

    return AnimatedOpacity(
      opacity: isPlayerTurn ? 1 : 0,
      duration: const Duration(seconds: 1),
      child: AbsorbPointer(
        absorbing: isPlayerTurn ? false : true,
        child: Column(
          children: [
            actionButton('Hit', Colors.teal.shade300.withOpacity(0.8), () {
              provider.playerAction(PlayerAction.hit);
            }),
            actionButton('Stand', Colors.red.shade300.withOpacity(0.8), () {
              provider.playerAction(PlayerAction.stand);
            }),
            actionButton('Split', Colors.amber.shade300.withOpacity(0.8), () {
              provider.playerAction(PlayerAction.split);
            }),
            // actionButton('Insurance', Colors.green.shade300.withOpacity(0.8)),
          ],
        ),
      ),
    );
  }

  ElevatedButton actionButton(
      String text, Color color, VoidCallback onPressed) {
    return ElevatedButton(
      onPressed: onPressed,
      style:
          ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(color)),
      child: Text(text),
    );
  }
}

class CardBuilt extends StatefulWidget {
  const CardBuilt(
      {Key? key,
      required this.leftCard,
      required this.bottomCard,
      required this.card})
      : super(key: key);

  final double leftCard;
  final double bottomCard;
  final ACard card;

  @override
  State<CardBuilt> createState() => _CardBuiltState();
}

class _CardBuiltState extends State<CardBuilt> {
  final _controller = FlipCardController();
  late GameManager provider;
  @override
  void initState() {
    super.initState();
    provider = context.read<GameManager>();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => widget.card.backSize ? _flipCard() : null);
    if (provider.dealer.hand.length == 2 && provider.player.hand.length == 2) {
      provider.addListener(_createProviderListener);
    }
  }

  _createProviderListener() {
    if (provider.isFlipTrumpCard == true) {
      _flipCard();
    }
  }

  _flipCard() async {
    Future.delayed(const Duration(milliseconds: 1000), () {
      _controller.toggleCard();
    });
  }

  @override
  void dispose() {
    provider.removeListener(_createProviderListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
      duration: const Duration(milliseconds: 500),
      left: widget.card.cardPositioned.left != 0
          ? widget.card.cardPositioned.left
          : widget.leftCard,
      bottom: widget.card.cardPositioned.bottom != 0
          ? widget.card.cardPositioned.bottom
          : widget.bottomCard,
      child: FlipCard(
        controller: _controller,
        fill: Fill.fillBack,
        direction: FlipDirection.HORIZONTAL,
        flipOnTouch: false,
        front: Image.asset(
          "assets/card_face_down.png",
          width: widthCard,
        ),
        back: Column(
          children: [
            // Text(widget.card.toString()),
            CardWidget(
              text: widget.card.name,
              cardType: widget.card.type,
            ),
          ],
        ),
      ),
    );
  }
}

class PlayerBuild extends StatelessWidget {
  const PlayerBuild(
      {super.key,
      required this.centerLeftCard,
      required this.cennterBottomCard});
  final double centerLeftCard;
  final double cennterBottomCard;

  @override
  Widget build(BuildContext context) {
    return Consumer<GameManager>(
      builder: (context, value, child) {
        print("rebuild player");
        return Stack(
          children: [
            for (var i = 0; i < value.player.hand.length; i++)
              CardBuilt(
                leftCard: centerLeftCard,
                bottomCard: cennterBottomCard,
                card: value.player.hand.cardList[i],
              ),
          ],
        );
      },
    );
  }
}

class DealerBuild extends StatelessWidget {
  const DealerBuild(
      {super.key,
      required this.centerLeftCard,
      required this.cennterBottomCard});
  final double centerLeftCard;
  final double cennterBottomCard;

  @override
  Widget build(BuildContext context) {
    return Consumer<GameManager>(
      builder: (context, value, child) {
        print("rebuild dealer");

        return Stack(
          children: [
            for (var i = 0; i < value.dealer.hand.length; i++)
              CardBuilt(
                leftCard: centerLeftCard,
                bottomCard: cennterBottomCard,
                card: value.dealer.hand.cardList[i],
              ),
          ],
        );
      },
    );
  }
}
