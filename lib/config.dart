import 'package:blackjack_app/models/a_card.dart';
import 'package:blackjack_app/utils.dart';

List<String> cardNumber = [
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "J",
  "Q",
  "K"
];

List<ACard> cardDesk = [
  ...cardNumber.map((card) => ACard(card, CardType.clubs)).toList(),
  ...cardNumber.map((card) => ACard(card, CardType.diamonds)).toList(),
  ...cardNumber.map((card) => ACard(card, CardType.hearts)).toList(),
  ...cardNumber.map((card) => ACard(card, CardType.spades)).toList(),
];

const widthCard = 70.0;
const heightCard = 110.0;
