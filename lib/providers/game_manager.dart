import 'package:blackjack_app/config.dart';
import 'package:blackjack_app/models/a_card.dart';
import 'package:blackjack_app/models/hand.dart';
import 'package:blackjack_app/models/player.dart';
import 'package:blackjack_app/providers/player_role.dart';
import 'package:blackjack_app/stack_data.dart';
import 'package:flutter/material.dart';

class GameManager with ChangeNotifier {
  final Player _playerInfo;
  GameManager(this._playerInfo);

  late StackData<ACard> cardDeskInGame;
  late Size screenSize;
  PlayerRole player = PlayerRole(PlayerType.player1);
  PlayerRole dealer = PlayerRole(PlayerType.dealer);
  PlayerType turnCurrent = PlayerType.player1;
  CardPositioned? cardPositioned;
  GameState gameCurrentStatus = GameState.init;
  String? result;
  bool isFlipTrumpCard = false;
  String playerScore = "0";
  String dealerScore = "0";

  Player get playerInfo => _playerInfo;

  initGame(Size size) async {
    await resetGame(size);
    for (var i = 0; i < 2; i++) {
      ACard playerCard = cardDeskInGame.pop();
      await dealsCards(player, playerCard, () => print('player'));
      ACard dealerCard = cardDeskInGame.pop();
      if (i == 1) {
        dealerCard.backSize = false;
      }
      await dealsCards(dealer, dealerCard, () => print('dealer1'));
    }

    setGameCurrentStatus(GameState.playerTurn);
    checkBlackjack();
  }

  resetGame(Size size) async {
    shuffleCards();
    setGameCurrentStatus(GameState.dealCard);
    screenSize = size;
    player = PlayerRole(PlayerType.player1);
    dealer = PlayerRole(PlayerType.dealer);
    playerScore = "0";
    dealerScore = "0";
    notifyListeners();
    await Future.delayed(const Duration(seconds: 1));
  }

  shuffleCards() {
    final fullCardDesk = cardDesk;
    fullCardDesk.shuffle();
    cardDeskInGame = StackData<ACard>(fullCardDesk);
  }

  dealsCards(PlayerRole playerReceived, ACard card, VoidCallback cb) async {
    // cb();
    playerReceived.hit(card);
    notifyListeners();
    await Future.delayed(const Duration(milliseconds: 1000));
    switch (playerReceived.playerType) {
      case PlayerType.player1:
        moveCard(playerReceived.hand, 35, 10);
        break;
      case PlayerType.dealer:
        moveCard(playerReceived.hand, 35, screenSize.height - heightCard - 35);
        break;
      default:
    }
    notifyListeners();
    playerScore = player.hand.scoreText;
    if (dealer.hand.length != 2) {
      dealerScore = dealer.hand.scoreText;
    }
  }

  moveCard(Hand hand, double space, double positionY) {
    final positionX = hand.length;
    hand.lastCard.cardPositioned = CardPositioned(positionX * space, positionY);
  }

  setGameCurrentStatus(GameState gameState) {
    gameCurrentStatus = gameState;
    notifyListeners();
  }


  playerAction(PlayerAction playerAction) async {
    switch (playerAction) {
      case PlayerAction.hit:
        setGameCurrentStatus(GameState.dealCard);
        await dealsCards(player, cardDeskInGame.pop(), () {});
        setGameCurrentStatus(GameState.playerTurn);
        checkPlayerBust();
        break;
      case PlayerAction.stand:
        await dealerAction();
        resultGame();
        break;
      case PlayerAction.double:
        break;
      case PlayerAction.insurance:
        break;
      case PlayerAction.split:
        await playerSplit();
        break;
      default:
    }
  }

  playerSplit() async {
    player.split();
    notifyListeners();
  }

  playerSwapHand(CurrentHand newCurrentHand) {
    player.swapHand(newCurrentHand);
    notifyListeners();
  }

  dealerAction() async {
    setGameCurrentStatus(GameState.dealerTurn);
    await flipTrumpCard();
    while (dealer.hand.scoreTotal < 17) {
      await Future.delayed(const Duration(milliseconds: 1000));
      await dealsCards(dealer, cardDeskInGame.pop(), () {});
    }
    await Future.delayed(const Duration(milliseconds: 1000));
  }

  showResult(String resultGame) { 
    result = resultGame;
    notifyListeners();
    result = null;
  }

  checkBlackjack() {
    checkPush();
    if (player.hand.scoreTotal == 21) {
      showResult("You win! \nPlayer's cards are blackjack");
    }

    if (dealer.hand.scoreTotal == 21) {
      final lastCard = dealer.hand.lastCard;
      lastCard.backSize = false;
      notifyListeners();
      showResult("You lose! Dealer's cards are blackjack");
    }
  }

  checkPlayerBust() {
    if (player.hand.isHandBust) {
      showResult("You Lose");
    }
  }

  checkPush() {
    if (player.hand.scoreTotal == dealer.hand.scoreTotal) {
      showResult("Push!");
    }
  }

  resultGame() {
    if (dealer.hand.scoreTotal > 21) {
      showResult("You win");
    } else {
      compareCards();
    }
  }

  compareCards() {
    if (player.hand.scoreTotal > dealer.hand.scoreTotal) {
      showResult("You win");
    } else {
      showResult("You Lose");
    }
  }

  applyRules() async {
    await Future.delayed(const Duration(milliseconds: 2000));
    checkPlayerBust();
  }

  flipTrumpCard() async {
    dealer.hand.lastCard.backSize = true;
    isFlipTrumpCard = true;
    notifyListeners();
    isFlipTrumpCard = false;
    await Future.delayed(const Duration(milliseconds: 1200));
    dealerScore = dealer.hand.scoreText;
    notifyListeners();
  }
}

enum PlayerType { dealer, player1, player2, player3, player4 }

enum PlayerAction { hit, stand, double, insurance, split }

enum GameState { init, dealCard, playerTurn, dealerTurn }
