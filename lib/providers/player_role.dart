import 'package:blackjack_app/models/a_card.dart';
import 'package:blackjack_app/models/hand.dart';
import 'package:blackjack_app/providers/game_manager.dart';
import 'package:flutter/material.dart';

class PlayerRole with ChangeNotifier {
  Hand hand = Hand();
  Hand tempHand = Hand();
  bool isSplitedCard = false;
  CurrentHand currentHand = CurrentHand.c1;

  final PlayerType playerType;

  PlayerRole(this.playerType);

  hit(ACard card) async {
    hand.addCard(card);
  }

  split() {
    final lastCard = hand.lastCard;
    tempHand.addCard(lastCard);
    hand.removeCard(lastCard);
    tempHand.cardList.first.setCardPositioned(hand.firstCard.cardPositioned);
    isSplitedCard = true;
  }

  swapHand(CurrentHand newCurrentHand) {
    if (currentHand != newCurrentHand) {
      currentHand = newCurrentHand;
      final temp = hand;
      hand = tempHand;
      tempHand = temp;
    }
  }
}

enum CurrentHand { c1, c2 }
