import 'package:flutter/material.dart';

enum CardType { spades, hearts, diamonds, clubs }

extension CardTypeExt on CardType {
  String get type {
    switch (this) {
      case CardType.clubs:
        return "♣";

      case CardType.hearts:
        return "♥";

      case CardType.diamonds:
        return "♦";

      case CardType.spades:
        return "♠";
    }
  }

  Color get color {
    if (this == CardType.clubs || this == CardType.spades) {
      return Colors.black;
    }
    return Colors.red;
  }
}

