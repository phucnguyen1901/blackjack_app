import 'package:flutter/material.dart';
import 'package:flutter_circular_text/circular_text.dart';

class HalfCircularText extends StatelessWidget {
  const HalfCircularText({super.key});

  @override
  Widget build(BuildContext context) {
    return CircularText(
      children: [
        TextItem(
          text: Text(
            "Blackjack pays 3 to 2".toUpperCase(),
            style: const TextStyle(
              fontSize: 20,
              color: Colors.amberAccent,
              fontWeight: FontWeight.bold,
            ),
          ),
          space: 7,
          startAngle: 90,
          startAngleAlignment: StartAngleAlignment.center,
          direction: CircularTextDirection.anticlockwise,
        ),
      ],
      radius: 150,
      position: CircularTextPosition.inside,
    );
  }
}
