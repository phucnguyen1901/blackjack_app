class StackData<T> {
  final List<T> dataList;

  StackData(this.dataList);

  T pop() {
    final data = dataList.removeAt(0);
    return data;
  }
}
