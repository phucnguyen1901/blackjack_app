import 'package:blackjack_app/config.dart';
import 'package:blackjack_app/utils.dart';
import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {
  const CardWidget({super.key, required this.cardType, required this.text});
  final CardType cardType;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: widthCard,
          height: heightCard,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(3),
          ),
        ),
        Positioned(
          left: 0,
          top: 0,
          child: Column(
            children: [
              Text(
                text,
                style: TextStyle(color: cardType.color),
              ),
              Text(cardType.type)
            ],
          ),
        ),
        Positioned(
            right: 0,
            bottom: 0,
            child: RotatedBox(
              quarterTurns: 2,
              child: Column(
                children: [
                  Text(
                    text,
                    style: TextStyle(color: cardType.color),
                  ),
                  Text(cardType.type)
                ],
              ),
            )),
        Text(
          cardType.type,
          style: const TextStyle(fontSize: 30),
        )
      ],
    );
  }
}
