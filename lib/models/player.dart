class Player {
  final String name;
  final BigInt wallet;

  Player(this.name, this.wallet);
}
