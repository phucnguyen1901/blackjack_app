import 'package:blackjack_app/models/a_card.dart';
import 'package:collection/collection.dart';

class Hand {
  List<ACard> cardList = [];
  bool isHandStanded = false;

  addCard(ACard card) {
    cardList.add(card);
  }

  removeCard(ACard card) {
    cardList.remove(card);
  }

  setStand() {
    isHandStanded = true;
  }

  String get scoreText {
    final aceCard = cardList.firstWhereOrNull((element) => element.name == "1");
    if (aceCard != null) {
      if (scoreTotal + 10 < 22) {
        return "${scoreTotal.toString()}/${scoreTotal + 10}";
      } else {
        return scoreTotal.toString();
      }
    }
    return scoreTotal.toString();
  }

  int get length => cardList.length;

  ACard get lastCard => cardList.last;
  ACard get firstCard => cardList.first;

  int get scoreTotal {
    final totalScore = cardList.fold(0, (previousValue, element) {
      int value;
      if (element.name == "J" || element.name == "Q" || element.name == "K") {
        value = 10;
      } else {
        value = int.parse(element.name);
      }
      return previousValue + value;
    });

    return totalScore;
  }

  bool get isSoftHand {
    final aceCard = cardList.firstWhereOrNull((element) => element.name == "1");
    if (aceCard != null) {
      return true;
    }
    return false;
  }

  bool get isHandBust {
    return scoreTotal > 21;
  }
}

class HandSplitting {
  final Hand hand1;
  final Hand hand2;

  HandSplitting(this.hand1, this.hand2);
}
