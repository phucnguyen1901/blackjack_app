import 'package:blackjack_app/utils.dart';

class ACard {
  final String name;
  final CardType type;
  CardPositioned cardPositioned = CardPositioned(0, 0);
  bool backSize = true;

  ACard(
    this.name,
    this.type,
  );

  setCardPositioned(CardPositioned newCardPositioned) {
    cardPositioned = newCardPositioned;
  }

  resetCardPositioned() {
    cardPositioned = CardPositioned(0, 0);
  }

  @override
  String toString() {
    return "$name ${type.type} / $backSize";
  }
}

class CardPositioned {
  final double left;
  final double bottom;

  CardPositioned(this.left, this.bottom);

  @override
  String toString() {
    return "left: $left, right: $bottom";
  }
}
